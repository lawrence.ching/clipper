package me.imlc.tools.clipper

import com.github.salomonbrys.kodein.*
import com.google.common.eventbus.EventBus
import org.slf4j.LoggerFactory
import java.awt.Toolkit

val HTTP_PORT = "HTTP_PORT"
val HTTP_HOST = "HTTP_HOST"

val defaultWindowsHotKeyServiceModule = Kodein.Module {
    bind() from singleton { WindowsHotKeyService.defaultKeyMasterProvider() }
    bind() from singleton { WindowsHotKeyServiceConfig() }
    bind() from singleton { WindowsHotKeyService(instance(), instance(), instance(), instance()) }
}

val defaultHttpServiceModule = Kodein.Module {
    constant(HTTP_PORT) with 8080
    constant(HTTP_HOST) with "localhost"
    bind() from singleton { HttpService(instance(HTTP_PORT), instance(HTTP_HOST), instance(), instance(), instance()) }
}

fun getKodein(
        windowsHotKeyServiceModule: Kodein.Module = defaultWindowsHotKeyServiceModule,
        httpServiceModule: Kodein.Module = defaultHttpServiceModule
): Kodein {
    return Kodein {
        bind() from singleton { LoggerFactory.getLogger("Main") }
        bind() from singleton { EventBus() }
        bind() from singleton { ClipboardService(instance(), instance(), instance(), instance()) }
        bind() from singleton { Store(instance()) }
        bind() from singleton { Toolkit.getDefaultToolkit().systemClipboard }
        bind() from singleton { ClipboardDelegate(instance(), instance()) }

        import(windowsHotKeyServiceModule)
        import(httpServiceModule)
    }
}