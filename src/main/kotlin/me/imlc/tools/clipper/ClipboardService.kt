package me.imlc.tools.clipper

import com.google.common.eventbus.EventBus
import com.google.common.eventbus.Subscribe
import com.google.common.util.concurrent.AbstractExecutionThreadService
import me.imlc.tools.clipper.events.SetClipboardContentByStoreIndexEvent
import org.slf4j.Logger

class ClipboardService(
        private val logger: Logger,
        private val store: Store,
        private val clipboard: ClipboardDelegate,
        private val bus: EventBus
) : AbstractExecutionThreadService() {

    override fun run() {
        while (isRunning) {
            val currentContent = clipboard.getContents()

            if (!store.hasContent(currentContent)) {
                logger.info("Detected new content: ${currentContent.string()}")
                store.add(currentContent)
            }

            Thread.sleep(1000)
        }
    }

    @Subscribe
    fun onSetClipboardContentByStoreIndex(e: SetClipboardContentByStoreIndexEvent) {
        logger.info("Received event SetClipboardContentByStoreIndexEvent with index ${e.index}")
        setClipboardContentByStoreIndex(e.index)
    }

    fun setClipboardContentByStoreIndex(index: Int) {
        try {
            val transferable = store.get(index)
            clipboard.setContents(transferable!!)
        } catch (e: IndexOutOfBoundsException) {
            throw IndexOutOfBoundsException("Clipper doesn't contain content at index $index")
        }
    }

    override fun startUp() {
        bus.register(this)
    }

    override fun shutDown() {
        bus.unregister(this)
    }
}