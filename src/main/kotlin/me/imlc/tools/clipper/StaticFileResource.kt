package me.imlc.tools.clipper

import org.apache.commons.io.FileUtils
import java.io.File
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.Produces
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("")
class StaticFileResource {

    @GET
    @Produces(MediaType.TEXT_HTML)
    fun getIndexHtml(): String {
        val indexHtml = File("asset/index.html")

        if(indexHtml.exists()) {
            return FileUtils.readFileToString(indexHtml, "UTF-8")
        } else {
            return "<h1>Unable to found index.html for given path ${indexHtml.absolutePath}"
        }
    }

    @GET
    @Path("/{fileName: .*ico}")
    @Produces("image/x-icon")
    fun getFavicon(): Response {
        return Response.ok(FileUtils.readFileToByteArray(File("asset/favicon.ico"))).build()
    }
}