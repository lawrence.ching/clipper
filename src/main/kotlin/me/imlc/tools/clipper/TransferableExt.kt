package me.imlc.tools.clipper

import java.awt.Image
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.DataFlavor.imageFlavor
import java.awt.datatransfer.DataFlavor.stringFlavor
import java.awt.datatransfer.Transferable
import java.awt.image.BufferedImage
import java.io.ByteArrayOutputStream
import javax.imageio.ImageIO

fun Transferable.string(): String? {
    val stringFlavor = DataFlavor.stringFlavor

    if (!this.isDataFlavorSupported(stringFlavor)) {
        return null
    }

    return this.getTransferData(stringFlavor) as String
}

fun Transferable.isEqual(target: Transferable): Boolean {
    when {
        target.isDataFlavorSupported(stringFlavor) -> return isEqualAsStringFlavor(target)
        target.isDataFlavorSupported(imageFlavor) -> return isEqualAsImageFlavor(target)
    }

    // Treats all unknown data flavors are same
    return true
}

fun Transferable.isEqualAsImageFlavor(target: Transferable): Boolean {
    if(!this.isDataFlavorSupported(imageFlavor)) {
        return false
    }

    if(!target.isDataFlavorSupported(imageFlavor)) {
        return false
    }

    val thisImage = this.getTransferData(DataFlavor.imageFlavor) as Image
    val targetImage = target.getTransferData(DataFlavor.imageFlavor) as Image

    return (thisImage.getWidth(null) == targetImage.getWidth(null))
            && (thisImage.getHeight(null) == targetImage.getHeight(null))
}

fun Transferable.isEqualAsStringFlavor(target: Transferable): Boolean {
    if(!this.isDataFlavorSupported(stringFlavor)) {
        return false
    }

    if(!target.isDataFlavorSupported(stringFlavor)) {
        return false
    }

    val data1 = this.getTransferData(stringFlavor) as? String
    val data2 = target.getTransferData(stringFlavor) as? String

    return data1 != null && data2 != null && data1==data2
}

fun Transferable.toImageByteArray(): ByteArray? {
    if(this.isDataFlavorSupported(imageFlavor)) {
        val image = this.getTransferData(DataFlavor.imageFlavor) as Image
        val bufferedImage = BufferedImage(image.getWidth(null), image.getHeight(null), BufferedImage.TYPE_INT_BGR)
        val graphics = bufferedImage.createGraphics()
        graphics.drawImage(image, null, null)
        val byteArrayOutputStream = ByteArrayOutputStream()
        ImageIO.write(bufferedImage, "png", byteArrayOutputStream)
        val byteArray = byteArrayOutputStream.toByteArray()
        byteArrayOutputStream.close()
        return byteArray
    }

    return null
}