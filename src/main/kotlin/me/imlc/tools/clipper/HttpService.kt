package me.imlc.tools.clipper

import com.google.common.util.concurrent.AbstractIdleService
import org.eclipse.jetty.server.Connector
import org.eclipse.jetty.server.Server
import org.eclipse.jetty.server.ServerConnector
import org.glassfish.hk2.utilities.binding.AbstractBinder
import org.glassfish.jersey.jetty.JettyHttpContainer
import org.glassfish.jersey.server.ContainerFactory
import org.glassfish.jersey.server.ResourceConfig
import org.slf4j.Logger

class HttpService(
        private val port: Int,
        private val host: String,
        private val logger: Logger,
        private val store: Store,
        private val clipboardService: ClipboardService
) : AbstractIdleService() {

    private lateinit var server: Server

    override fun startUp() {
        val resourceConfig = ResourceConfig(ClipboardResource::class.java, StaticFileResource::class.java)
        resourceConfig.register((object : AbstractBinder() {
            override fun configure() {
                bind<Logger>(logger).to(Logger::class.java)
                bind<Store>(store).to(Store::class.java)
                bind<ClipboardService>(clipboardService).to(ClipboardService::class.java)
            }
        }))

        val container = ContainerFactory.createContainer(JettyHttpContainer::class.java, resourceConfig)
        server = Server(JettyConnectorThreadPool())

        val httpConnector = ServerConnector(server)
        httpConnector.port = port
        httpConnector.host = host

        server.connectors = arrayOf<Connector>(httpConnector)
        server.handler = container
        server.start()
    }

    override fun shutDown() {
        server.stop()
    }
}