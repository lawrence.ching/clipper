package me.imlc.tools.clipper.events

data class SetClipboardContentByStoreIndexEvent(val index: Int)