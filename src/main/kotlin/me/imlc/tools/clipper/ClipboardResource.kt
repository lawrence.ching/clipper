package me.imlc.tools.clipper

import org.json.JSONArray
import org.json.JSONObject
import org.slf4j.Logger
import java.awt.datatransfer.DataFlavor
import java.io.File
import javax.inject.Inject
import javax.ws.rs.*
import javax.ws.rs.core.MediaType
import javax.ws.rs.core.Response

@Path("/clipboard")
class ClipboardResource {

    @Inject
    private lateinit var store:Store

    @Inject
    private lateinit var logger: Logger

    @Inject
    private lateinit var clipboardService: ClipboardService

    @GET
    @Path("/{index}")
    @Produces("image/png")
    fun getImageFromClipboard(@PathParam("index") index: Int): Response {
        val transferable = store.get(index) ?: return Response.status(404).build()

        if(transferable.isDataFlavorSupported(DataFlavor.imageFlavor)) {
            return Response.ok(transferable.toImageByteArray())
                    .header("Cache-Control", "no-cache, no-store, must-revalidate")
                    .build()
        } else {
            return Response.status(404).build()
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    fun clipboardContent(): String {
        logger.debug("[GET] /clipboard")
        val contents = store.getAll()
        val jsonArray = JSONArray()

        contents.forEach { content ->
            when {
                content.isDataFlavorSupported(DataFlavor.stringFlavor) -> {
                    val obj = JSONObject()
                    obj.put("content", content.getTransferData(DataFlavor.stringFlavor))
                    obj.put("type", "string")
                    jsonArray.put(obj)
                }
                content.isDataFlavorSupported(DataFlavor.javaFileListFlavor) -> {
                    val obj = JSONObject()
                    val files = content.getTransferData(DataFlavor.javaFileListFlavor) as List<File>
                    obj.put("content", files.map { it.path }.joinToString(" "))
                    obj.put("type", "files")
                    jsonArray.put(obj)
                }
                content.isDataFlavorSupported(DataFlavor.imageFlavor) -> {
                    val obj = JSONObject()
                    obj.put("content", "//TODO: I should print the image url in this field")
                    obj.put("type", "image")
                    jsonArray.put(obj)
                }
            }
        }

        val json = JSONObject()
        json.put("status", 0)
        json.put("message", "success")
        json.put("data", jsonArray)
        return json.toString()
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    fun setClipboardContent(@FormParam("index") index:Int): String {
        logger.debug("[POST] /clipboard {\"index\": $index}")
        val transferable = store.get(index)

        val json = JSONObject()

        try {
            clipboardService.setClipboardContentByStoreIndex(index)
            json.put("status", 0)
            json.put("message", "success")
        } catch(e: IllegalArgumentException) {
            json.put("status", 1)
            json.put("message", e.message)
        }

        return json.toString()
    }
}