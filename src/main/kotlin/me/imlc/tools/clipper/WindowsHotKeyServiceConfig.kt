package me.imlc.tools.clipper

data class WindowsHotKeyServiceConfig(
        val toRegisterCtrlAlt1: Boolean = true,
        val toRegisterCtrlAlt2: Boolean = true,
        val toRegisterCtrlAlt3: Boolean = true,
        val toRegisterCtrlAlt4: Boolean = true,
        val toRegisterCtrlAlt5: Boolean = true,
        val toRegisterCtrlAlt6: Boolean = true,
        val toRegisterCtrlAlt7: Boolean = true,
        val toRegisterCtrlAlt8: Boolean = true
)