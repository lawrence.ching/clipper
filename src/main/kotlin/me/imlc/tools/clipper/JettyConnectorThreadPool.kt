package me.imlc.tools.clipper

import jersey.repackaged.com.google.common.util.concurrent.ThreadFactoryBuilder
import org.eclipse.jetty.util.thread.QueuedThreadPool
import org.glassfish.jersey.process.JerseyProcessingUncaughtExceptionHandler

class JettyConnectorThreadPool : QueuedThreadPool() {
    private val threadFactory = ThreadFactoryBuilder()
            .setNameFormat("jetty-http-server-%d")
            .setUncaughtExceptionHandler(JerseyProcessingUncaughtExceptionHandler())
            .build()

    override fun newThread(runnable: Runnable): Thread {
        return threadFactory.newThread(runnable)
    }
}