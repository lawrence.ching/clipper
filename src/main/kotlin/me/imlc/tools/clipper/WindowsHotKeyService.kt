package me.imlc.tools.clipper

import com.google.common.eventbus.EventBus
import com.google.common.util.concurrent.AbstractIdleService
import com.tulskiy.keymaster.common.Provider
import me.imlc.tools.clipper.events.SetClipboardContentByStoreIndexEvent
import org.slf4j.Logger
import javax.inject.Inject
import javax.swing.KeyStroke

private val CTRL = "control"
private val ALT = "alt"

private fun getKeyStroke(vararg hotKeys: String): KeyStroke {
    return KeyStroke.getKeyStroke(hotKeys.joinToString(" "))
}

val CTRL_ALT_1 = getKeyStroke(CTRL, ALT, "1")
val CTRL_ALT_2 = getKeyStroke(CTRL, ALT, "2")
val CTRL_ALT_3 = getKeyStroke(CTRL, ALT, "3")
val CTRL_ALT_4 = getKeyStroke(CTRL, ALT, "4")
val CTRL_ALT_5 = getKeyStroke(CTRL, ALT, "5")
val CTRL_ALT_6 = getKeyStroke(CTRL, ALT, "6")
val CTRL_ALT_7 = getKeyStroke(CTRL, ALT, "7")
val CTRL_ALT_8 = getKeyStroke(CTRL, ALT, "8")


class WindowsHotKeyService(private val provider: Provider,
                           private val config: WindowsHotKeyServiceConfig,
                           private val eventBus: EventBus,
                           private val logger: Logger)
    : AbstractIdleService() {

    private fun registerHotKey(keyStroke: KeyStroke, toRegister: Boolean, storeIndex: Int) {
        if (toRegister) provider.register(keyStroke, {
            _ -> eventBus.post(SetClipboardContentByStoreIndexEvent(storeIndex))
        })
    }

    override fun startUp() {
        logger.info("Starting up ${this.serviceName()}")

        registerHotKey(CTRL_ALT_1, config.toRegisterCtrlAlt1, 0)
        registerHotKey(CTRL_ALT_2, config.toRegisterCtrlAlt2, 1)
        registerHotKey(CTRL_ALT_3, config.toRegisterCtrlAlt3, 2)
        registerHotKey(CTRL_ALT_4, config.toRegisterCtrlAlt4, 3)
        registerHotKey(CTRL_ALT_5, config.toRegisterCtrlAlt5, 4)
        registerHotKey(CTRL_ALT_6, config.toRegisterCtrlAlt6, 5)
        registerHotKey(CTRL_ALT_7, config.toRegisterCtrlAlt7, 6)
        registerHotKey(CTRL_ALT_8, config.toRegisterCtrlAlt8, 7)

        logger.info("Started up ${this.serviceName()}")
    }

    override fun shutDown() {
        logger.info("Shutting down ${this.serviceName()}")
        provider.reset()
        provider.stop()
        logger.info("Shutted down ${this.serviceName()}")
    }

    companion object {
        fun defaultKeyMasterProvider(): Provider {
            return Provider.getCurrentProvider(false);
        }
    }
}