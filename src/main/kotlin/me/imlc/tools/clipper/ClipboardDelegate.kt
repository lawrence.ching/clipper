package me.imlc.tools.clipper

import org.slf4j.Logger
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.ClipboardOwner
import java.awt.datatransfer.Transferable

class ClipboardDelegate(
        private val clipboard: Clipboard,
        private val logger: Logger
) : ClipboardOwner {

    fun getContents(): Transferable {
        return clipboard.getContents(null)
    }

    fun setContents(transferable: Transferable) {
       clipboard.setContents(transferable, this)
    }

    override fun lostOwnership(clipboard: Clipboard?, contents: Transferable?) {
        logger.debug("ClipboardDelegate: Lost ownership")
    }
}