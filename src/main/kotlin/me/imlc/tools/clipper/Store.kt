package me.imlc.tools.clipper

import org.slf4j.Logger
import java.awt.datatransfer.Transferable
import java.util.concurrent.LinkedBlockingDeque

class Store(logger: Logger) {
    private val logger = logger
    var maxContent = 10
    private val contents = LinkedBlockingDeque<Transferable>()

    val size: Int
        get() = contents.size

    fun add(newContent: Transferable) {
        if(contents.contains(newContent)) {
            logger.debug("Unable to add content because this content already exists")
            return
        }

        if(contents.size >= maxContent) {
            logger.debug("Store is full, remove last one to add new one.")
            contents.pollLast()
        }

        contents.push(newContent)
    }

    fun get(index: Int): Transferable? {
        return contents.elementAt(index)
    }

    fun getAll(): List<Transferable> {
        return contents.toList()
    }

    fun hasContent(transferable: Transferable): Boolean {
        return contents.find { trans -> trans.isEqual(transferable) }!=null
    }
}