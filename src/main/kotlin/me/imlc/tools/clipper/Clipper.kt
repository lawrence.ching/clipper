package me.imlc.tools.clipper

import com.github.salomonbrys.kodein.Kodein
import com.github.salomonbrys.kodein.instance
import com.google.common.util.concurrent.AbstractIdleService
import com.google.common.util.concurrent.ServiceManager
import org.slf4j.Logger

class Clipper(
        private val logger: Logger,
        private val windowsHotKeyService: WindowsHotKeyService,
        private val clipboardService: ClipboardService,
        private val httpService: HttpService
) : AbstractIdleService() {

    constructor(kodein: Kodein) : this(kodein.instance(), kodein.instance(), kodein.instance(), kodein.instance())

    override fun startUp() {
        logger.info("Starting up Clipper...")

        val serviceManager = ServiceManager(listOf(
                windowsHotKeyService,
                clipboardService,
                httpService))

        logger.info("Starting all services...")
        serviceManager.startAsync()
        serviceManager.awaitHealthy()

        logger.info("Started up Clipper...")
    }

    override fun shutDown() {
        logger.info("Shutting down Clipper...")
    }
}

fun main(args: Array<String>) {
    val clipper = Clipper(getKodein())
    clipper.start()

    Runtime.getRuntime().addShutdownHook(Thread {
        clipper.stop()
    })
}

