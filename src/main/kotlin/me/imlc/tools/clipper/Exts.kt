package me.imlc.tools.clipper

import com.google.common.util.concurrent.Service
import org.apache.http.HttpEntity
import org.apache.http.util.EntityUtils
import java.util.concurrent.TimeUnit

fun Service.start(timeout: Long = 5) {
    this.startAsync()
    this.awaitRunning(timeout, TimeUnit.MINUTES)
}

fun Service.stop(timeout: Long = 5) {
    this.stopAsync()
    this.awaitTerminated(timeout, TimeUnit.MINUTES)
}

fun HttpEntity.string(): String? {
    return EntityUtils.toString(this, "UTF-8")
}