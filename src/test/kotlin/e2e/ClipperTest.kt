package e2e

import com.github.salomonbrys.kodein.*
import me.imlc.tools.clipper.*
import org.apache.http.HttpStatus.SC_OK
import org.apache.http.NameValuePair
import org.apache.http.client.entity.UrlEncodedFormEntity
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.impl.client.CloseableHttpClient
import org.apache.http.impl.client.HttpClients
import org.apache.http.message.BasicNameValuePair
import org.assertj.core.api.Assertions.assertThat
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.skyscreamer.jsonassert.JSONAssert
import org.slf4j.LoggerFactory
import scaffold.getFreePort
import scaffold.setString
import java.awt.Toolkit
import java.util.*

class ClipperTest {
    private lateinit var httpClient: CloseableHttpClient
    private lateinit var kodein: Kodein
    private lateinit var clipper: Clipper
    private lateinit var initContent: String
    private lateinit var clipboardDelegate: ClipboardDelegate
    private var port: Int = 80

    @Test
    fun canRetrieveClipboardContent() {
        val httpGet = HttpGet("http://localhost:$port/clipboard")
        val response = httpClient.execute(httpGet)
        assertThat(response.statusLine.statusCode).isEqualTo(SC_OK)

        val content = response.entity.string()
        logger.info(content)
        JSONAssert.assertEquals("""
                {
                  "data": [
                    {
                      "type": "string",
                      "content": "$initContent"
                    }
                  ],
                  "message": "success",
                  "status": 0
                }
            """, content, false)
    }

    @Test
    fun canDetectClipboardChange() {
        clipboardDelegate.setString("2nd content")
        val httpGet = HttpGet("http://localhost:$port/clipboard")
        val response = httpClient.execute(httpGet)
        assertThat(response.statusLine.statusCode).isEqualTo(SC_OK)

        val content = response.entity.string()!!
        logger.info(content)
        JSONAssert.assertEquals("""
                {
                  "data": [
                    {
                      "type": "string",
                      "content": "2nd content"
                    },
                    {
                      "type": "string",
                      "content": "$initContent"
                    }
                  ],
                  "message": "success",
                  "status": 0
                }
            """, content, true)
    }

    @Test
    fun canSetClipboardContentByIndex() {
        clipboardDelegate.setString("2nd content")
        val httpPost = HttpPost("http://localhost:$port/clipboard")
        val params = ArrayList<NameValuePair>()
        params.add(BasicNameValuePair("index", "1"))
        httpPost.entity = UrlEncodedFormEntity(params)

        val response = httpClient.execute(httpPost).entity.string()
        logger.info(response)
        JSONAssert.assertEquals("""
            {"message":"success","status":0}
            """, response, true)

        assertThat(clipboardDelegate.getContents().string()).isEqualTo(initContent)
    }

    @Before
    fun setUp() {
        httpClient = HttpClients.createMinimal()
        port = getFreePort()
        initContent = Random().nextInt().toString(10)

        kodein = Kodein {
            extend(getKodein())
            constant(HTTP_PORT, overrides = true) with port
            bind(overrides = true) from singleton { logger }
        }

        clipboardDelegate = ClipboardDelegate(Toolkit.getDefaultToolkit().systemClipboard, kodein.instance())
        clipboardDelegate.setString(initContent)

        clipper = Clipper(kodein)
        clipper.startAsync()
        clipper.awaitRunning()
    }

    @After
    fun tearDown() {
        httpClient.close()
        clipper.stop()
    }

    companion object {
        val logger = LoggerFactory.getLogger("ClipperTest")
    }
}