package scaffold

import com.google.common.eventbus.EventBus
import com.google.common.eventbus.Subscribe
import me.imlc.tools.clipper.events.SetClipboardContentByStoreIndexEvent
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class LoggingEventBus(private val logger: Logger = LoggerFactory.getLogger("LoggingEventBus")): EventBus() {

    init {
        this.register(this)
    }

    @Subscribe
    fun onSetClipboardContentByStoreIndex(e: SetClipboardContentByStoreIndexEvent) {
        logger.info("Received event ${e.javaClass.simpleName} with index ${e.index}")
    }
}