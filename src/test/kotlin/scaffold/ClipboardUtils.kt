package scaffold

import me.imlc.tools.clipper.ClipboardDelegate
import java.awt.datatransfer.StringSelection


fun ClipboardDelegate.setString(content:String) {
    Thread.sleep(1050)
    this.setContents(StringSelection(content))
    Thread.sleep(1050)
}