package me.imlc.tools.clipper

import com.nhaarman.mockito_kotlin.*
import com.tulskiy.keymaster.common.HotKeyListener
import com.tulskiy.keymaster.common.Provider
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import scaffold.LoggingEventBus
import javax.swing.KeyStroke

class WindowsHotKeyService_WithAllDisableConfig_Test {
    private lateinit var logger: Logger
    private lateinit var keyMasterProvider: Provider
    private lateinit var loggingEventBus: LoggingEventBus
    private lateinit var windowsHotKeyService: WindowsHotKeyService
    private lateinit var windowsHotKeyServiceConfig: WindowsHotKeyServiceConfig

    @Test
    fun willNotListenToCtrlAlt1() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_1), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt2() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_2), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt3() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_3), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt4() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_4), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt5() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_5), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt6() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_6), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt7() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_7), isNotNull())
    }

    @Test
    fun willNotListenToCtrlAlt8() {
        verify(keyMasterProvider, never()).register(eq(CTRL_ALT_8), isNotNull())
    }

    @Before
    fun setUp() {
        logger = LoggerFactory.getLogger(WindowsHotKeyService_WithAllDisableConfig_Test::class.java)
        loggingEventBus = LoggingEventBus(logger)

        keyMasterProvider = mock()
        doNothing().`when`(keyMasterProvider).register(any<KeyStroke>(), any<HotKeyListener>())

        windowsHotKeyServiceConfig = WindowsHotKeyServiceConfig(false, false, false, false, false, false, false, false)
        windowsHotKeyService = WindowsHotKeyService(keyMasterProvider, windowsHotKeyServiceConfig, loggingEventBus, logger)

        windowsHotKeyService.start()
    }

    @After
    fun tearDown() {
        windowsHotKeyService.stop()
    }
}