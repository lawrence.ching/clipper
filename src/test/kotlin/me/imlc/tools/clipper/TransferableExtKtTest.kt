package me.imlc.tools.clipper

import org.assertj.core.api.Assertions.assertThat
import org.junit.Test
import java.awt.datatransfer.DataFlavor
import java.awt.datatransfer.StringSelection
import java.awt.datatransfer.Transferable
import java.net.URL
import javax.activation.DataHandler

class TransferableExtKtTest {
    @Test
    fun canReturnStringForStringFlavor() {
        val stringSelection = StringSelection("anyString")
        assertThat(stringSelection.string()).isEqualTo("anyString")
    }

    @Test
    fun returnNullForOtherFlavor() {
        val dataHandler = DataHandler(URL("http://localhost"))
        assertThat(dataHandler.string()).isNull()
    }

    @Test
    fun isEqualReturnTrueWhenCompareSameString() {
        val str1 = StringBuilder().append("some string here").toString()
        val str2 = StringBuilder().append("some string here").toString()

        val stringSelection1 = StringSelection(str1)
        val stringSelection2 = StringSelection(str2)
        assertThat(stringSelection1.isEqual(stringSelection2)).isTrue()
    }

    @Test
    fun isEqualReturnFalseWhenCompareDifferentString() {
        val str1 = StringBuilder().append("some string here").toString()
        val str2 = StringBuilder().append("different string here").toString()

        val stringSelection1 = StringSelection(str1)
        val stringSelection2 = StringSelection(str2)
        assertThat(stringSelection1.isEqual(stringSelection2)).isFalse()
    }

    @Test
    fun isEqualReturnTrueWhenCompareDifferentDataFlavor() {
        val stringSelection = StringSelection("string here")
        val otherTransferable = SomeTransferable()

        assertThat(stringSelection.isEqual(otherTransferable)).isTrue()
    }

    inner class SomeTransferable: Transferable {
        override fun getTransferData(flavor: DataFlavor?): Any {
            return true
        }

        override fun isDataFlavorSupported(flavor: DataFlavor?): Boolean {
            return false
        }

        override fun getTransferDataFlavors(): Array<DataFlavor> {
            return arrayOf(DataFlavor.allHtmlFlavor)
        }
    }
}