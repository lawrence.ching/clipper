package me.imlc.tools.clipper

import org.assertj.core.api.Assertions.assertThat
import org.junit.Before
import org.junit.Test
import org.slf4j.LoggerFactory
import java.awt.datatransfer.StringSelection

class StoreTest {
    private val logger = LoggerFactory.getLogger(StoreTest::class.java)

    @Test
    fun hasContainReturnTrueIfStoreContainsGiveString() {
        store.add(StringSelection("anyString"))
        assertThat(store.hasContent(StringSelection("anyString"))).isTrue()
    }

    @Test
    fun hasContainReturnFalseIfStoreDoesNotContainGiveString() {
        assertThat(store.hasContent(StringSelection("anyString"))).isFalse()
    }

    private lateinit var store: Store

    @Test
    fun canAddNewContent() {
        store.add(StringSelection("anyString"))
        val content = store.get(0) as StringSelection
        assertThat(content.string()).isEqualTo("anyString")
    }

    @Test
    fun canPushNewContentToQueueWhenQueueIsFull() {
        for(i in 0..9) {
            store.add(StringSelection("$i"))
        }

        assertThat(store.get(0)?.string()).isEqualTo("9")
        assertThat(store.get(9)?.string()).isEqualTo("0")

        store.add(StringSelection("any string"))

        assertThat(store.get(0)?.string()).isEqualTo("any string")
        assertThat(store.get(9)?.string()).isEqualTo("1")
    }

    @Before
    fun setUp() {
        store = Store(logger)
    }
}