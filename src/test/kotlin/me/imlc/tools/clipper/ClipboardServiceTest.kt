package me.imlc.tools.clipper

import com.google.common.eventbus.EventBus
import me.imlc.tools.clipper.events.SetClipboardContentByStoreIndexEvent
import org.assertj.core.api.Assertions.*
import org.awaitility.Awaitility.await
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.LoggerFactory
import scaffold.setString
import java.awt.Toolkit
import java.util.concurrent.TimeUnit.SECONDS


class ClipboardServiceTest {

    @Test
    fun canIgnoreRecordedString() {
        assertThat(store.size).isEqualTo(2)
        assertThat(store.get(0)?.string()).isEqualTo("2nd content")
        assertThat(store.get(1)?.string()).isEqualTo("1st content")

        clipboardDelegate.setString("1st content")

        assertThat(store.size).isEqualTo(2)
        assertThat(store.get(0)?.string()).isEqualTo("2nd content")
        assertThat(store.get(1)?.string()).isEqualTo("1st content")
    }

    @Test
    fun canDetectNewString() {
        assertThat(store.size).isEqualTo(2)
        
        clipboardDelegate.setString("new content")

        assertThat(store.get(0)?.string()).isEqualTo("new content")
        assertThat(store.get(1)?.string()).isEqualTo("2nd content")
        assertThat(store.get(2)?.string()).isEqualTo("1st content")
        assertThat(store.size).isEqualTo(3)
    }

    @Test
    fun canSubscribeSetClipboardContentByStoreIndexEvent() {
        eventBus.register(clipboardService)
        assertThat(clipboardDelegate.getContents().string()).isEqualTo("2nd content")
        eventBus.post(SetClipboardContentByStoreIndexEvent(1))
        assertThat(clipboardDelegate.getContents().string()).isEqualTo("1st content")
    }

    @Test
    fun throwExceptionIfSetContentByOutOfBoundaryIndex() {
        val outOfBoundaryIndex = 1000

        assertThatThrownBy { clipboardService.setClipboardContentByStoreIndex(outOfBoundaryIndex) }
                .isInstanceOf(IndexOutOfBoundsException::class.java)
                .hasMessageContaining("Clipper doesn't contain content at index $outOfBoundaryIndex")
    }

    private lateinit var store: Store

    private lateinit var clipboardDelegate: ClipboardDelegate

    private lateinit var eventBus: EventBus

    private lateinit var clipboardService: ClipboardService

    private lateinit var initContent: Unit

    @Before
    fun setUp() {
        store = Store(logger)
        clipboardDelegate = ClipboardDelegate(Toolkit.getDefaultToolkit().systemClipboard, logger)
        initContent = clipboardDelegate.setString("1st content")
        eventBus = EventBus()
        clipboardService = ClipboardService(logger, store, clipboardDelegate, eventBus)

        clipboardService.startAsync()
        clipboardService.awaitRunning()

        clipboardDelegate.setString("2nd content")

        await().atMost(5, SECONDS).until({
            store.size == 2
        })
    }

    @After
    fun tearDown() {
        clipboardService.stopAsync()
        clipboardService.awaitTerminated()
    }

    companion object {
        val logger = LoggerFactory.getLogger("ClipboardServiceTest")
    }
}