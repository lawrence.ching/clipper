package me.imlc.tools.clipper

import com.nhaarman.mockito_kotlin.*
import com.tulskiy.keymaster.common.HotKeyListener
import com.tulskiy.keymaster.common.Provider
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.slf4j.Logger
import scaffold.LoggingEventBus
import javax.swing.KeyStroke

class WindowsHotKeyServiceTest {
    private lateinit var logger: Logger
    private lateinit var keyMasterProvider: Provider
    private lateinit var loggingEventBus: LoggingEventBus
    private lateinit var windowsHotKeyService: WindowsHotKeyService
    private lateinit var windowsHotKeyServiceConfig: WindowsHotKeyServiceConfig

    @Test
    fun canSendSetClipboardContentByStoreIndexEvent() {
        var listener: HotKeyListener? = null
        verify(keyMasterProvider).register(eq(CTRL_ALT_1), check {
            //TODO: This code block will be called twice but I don't know why ...
            listener = it
        })
        listener?.onHotKey(null)
        verify(logger).info("Received event SetClipboardContentByStoreIndexEvent with index 0")
    }

    @Test
    fun canListenToCtrlAlt1() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_1), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt2() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_2), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt3() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_3), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt4() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_4), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt5() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_5), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt6() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_6), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt7() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_7), isNotNull())
    }

    @Test
    fun canListenToCtrlAlt8() {
        verify(keyMasterProvider).register(eq(CTRL_ALT_8), isNotNull())
    }

    @Before
    fun setUp() {
        logger = mock()
        loggingEventBus = LoggingEventBus(logger)

        keyMasterProvider = mock()
        doNothing().`when`(keyMasterProvider).register(any<KeyStroke>(), any<HotKeyListener>())

        windowsHotKeyServiceConfig = WindowsHotKeyServiceConfig(true, true, true, true, true, true, true, true)
        windowsHotKeyService = WindowsHotKeyService(keyMasterProvider, windowsHotKeyServiceConfig, loggingEventBus, logger)

        windowsHotKeyService.start()
    }

    @After
    fun tearDown() {
        windowsHotKeyService.stop()
    }
}