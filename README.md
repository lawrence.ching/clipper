# Clipper [![pipeline status](https://gitlab.com/lawrence.ching/clipper/badges/master/pipeline.svg)](https://gitlab.com/lawrence.ching/clipper/commits/master) [![coverage report](https://gitlab.com/lawrence.ching/clipper/badges/master/coverage.svg)](https://gitlab.com/lawrence.ching/clipper/commits/master)

Clipper is a lightweight clipboard tool that help you record the clipboard history.
There are so much tools to manage the clipboard in MacOS but few in Windows.
And I found nothing I can install on my company because of IT security reason.
Then I decide to create one.

It's build on Java and HTML. I believe running a Java project and open a html in Browser
is much easier and has lower risk than downloading and running an `.exe`.

## Requirement

* Java 8
* Chrome > 61
* External Internet Access (Will remove this requirement in the coming development)

## User Manual
Run 
`java -jar clipper.jar`
in console, then open `http://localhost:8080` in browser.

## IT Introduction

It's worth to introduce some details in IT aspect.
When you launch Clipper,
you will launch a service who scan you system clipboard continuously.
Further, Clipper will start a localhost-restricted HTTP server to serve the web UI.

The interaction between backend service and UI is base on HTTP.
    